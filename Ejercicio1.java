package ejercicio1;


/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.*; 
//al incorporar el caracter " * " se incluyen ya todas las clases.
import java.util.*;

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */

public class Ejercicio1 {
	
	
		final static String cad = "Bienvenido al programa";
		public static void main(String[] args) {
			
		int a;
		int b;			
		String cadena1;
		String cadena2;
		Scanner sc = new Scanner(System.in);
			
		System.out.println(cad);
		System.out.println("Introduce tu dni");
		cadena1 = sc.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = sc.nextLine();
			
		a=7; 
		b=16;
		int numeroc = 25;
		
		
		if(a>b||numeroc%5!=0&&(numeroc*3-1)>b/numeroc) {
		
		System.out.println("Se cumple la condición");
}
			
		
		numeroc = a+b*numeroc+b/a;
		
		
		String[] array = new String[7]; //Los arrays tienen los corchetes [ ] unidos a su tipo de datos:
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
			
			
		recorrer_array(array);
}
		
	
		
		static void recorrer_array(String vectordestrings[])  {
			
		for (int dia=0; dia<7; dia++) {
			
		System.out.println("El dia de la semana en el que te encuentras ["+(dia+1)+"-7] es el dia: "+vectordestrings[dia]);
			
			}
		}
		
	}


