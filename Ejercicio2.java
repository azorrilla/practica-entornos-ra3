package ejercicio2;

import java.io.*;

//al importar e incluir el caracter " * " ya se incluyen todas las clases.

import java.util.*;


public class Ejercicio2 {


	static Scanner as;
	
//un Scanner nunca debe ser llamado con un unico caracter.
	
	public static void main(String[] args) {
	
//el corchete { siempre debe ir seguido del metodo main.
		
	as = new Scanner(System.in);
		
	int n;
	int cantidad_maxima_alumnos= 10;
		
//cada variable que tengamos, debe ser declarada en una l�nea distinta cada una.
//el valor que le demos a cada variable (si se lo damos) debe estar indicado en la misma l�nea.
		
		
		
	int arrays[] = new int[10];
		
	for(n=0; n<10; n++){
			
		System.out.println("Introduce nota media de alumno");
		
		arrays[n] = as.nextInt();
		
}	
		
		System.out.println("El resultado es: " + recorrer_array(arrays));
		
		as.close();
		
//los corchetes de cierre deben estar al principio de la linea o lineas siguientes.
		
}
	
	
	static double recorrer_array (int vector[]) {
		
		double c = 0;
		
		for (int a = 0; a < 10; a++) {
			
			c = c+vector[a];
	
/*
 * debemos dejar espacio entre las l�neas para hacer m�s entendible el c�digo. 
 * los espacios en blanco mejoran la legibilidad. Se deben colocar entre operadores, 
 * despu�s de los puntos y coma de los bucles for, y despu�s de los operadores de asignaci�n.
 */
			
		}
		
	return c/10;
	
	}
	
}